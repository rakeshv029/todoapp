 
import React, {   Component }  from 'react';
import {   FlatList,TextInput,Button, StyleSheet, Text, View, Alert  } from 'react-native';
  
export default class App extends Component {
     
   constructor(props) { 
        super(props); 
        console.disableYellowBox = true;
        this.array = [
        ], 
        this.state = { 
          taskList: [], 
          input_task: '' 
        } 
      } 
      RemoveItem(item) { 
        Alert.alert(item);  
        this.setState({
          data: this.state.taskList.filter((_, i) => i !== item)
        }); 
      } 
    joinData = () => { 
          let getCurrentData = JSON.parse(JSON.stringify(this.state.taskList)); 
          let id = getCurrentData.length+1;
          getCurrentData.push({id: id,title: this.state.input_task}); 
          this.setState({ taskList: getCurrentData });
          this.textInput.clear()
      } 
  render() {
    
    return(
    <View style={styles.container}> 
        <Text style={styles.heading}> Todo APP </Text> 
      
    <View style={styles.fixToText}>
        <TextInput
            ref={input => { this.textInput = input }} 
            style={styles.inputtext}
            placeholder="Type here to add task"
            onChangeText={(text)=>{
              this.setState({input_task: text})
            }}
          /> 
        <Button
          title="Add Task" color="#841584" 
          onPress={this.joinData}
        />
      </View>  
      <FlatList 
          data={this.state.taskList}
          width='100%'
          extraData={this.state.taskList}
          keyExtractor={(index) => index.toString()} 
          renderItem={({ item }) => 
                  <View style={styles.counter}> 
                    <Text style={styles.remove} onPress={this.RemoveItem.bind(this, item.title)}> {item.id} </Text>
                    <Text style={styles.item} > {item.title} </Text>
                    <Text style={styles.remove} onPress={this.RemoveItem.bind(this, item.id)}> X </Text>
                  </View>}    
      />
      
    </View>
    )};
  }


const styles = StyleSheet.create({
  container: {
    flex: 1, 
   paddingTop: 22,
    backgroundColor: '#e0ffff', 
    justifyContent: 'center',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  inputtext: {
    width:260,
    padding:5,
    fontSize: 20,
    height: 45,
    borderColor: 'gray',
    borderWidth: 1
  },
  heading: {
    width:250,
    padding: 0,
    fontSize: 20,
    height: 45, 
    alignItems:"center", 
  },
  addbtn: {
    width:50,
    padding: 0,
    fontSize: 20,
    height: 45,  
  },
  remove: {
    width:50,
    padding: 0,
    fontSize: 20,
    height: 45,  
    color: 'red', 
  },
  fixToText: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  counter: {
    padding: 2, 
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
}); 